
#include "cell.h"

using namespace oxygine;

Cell::Cell(const Point &boardCoordinate, int cellSize)
    : m_boardCoordinate(boardCoordinate)
{
    int offsetX = m_boardCoordinate.x * 2;
    int offsetY = m_boardCoordinate.y * 2;
    int posX = m_boardCoordinate.x * cellSize + offsetX;
    int posY = m_boardCoordinate.y * cellSize + offsetY;

    initView(Point(posX, posY), cellSize);

    auto callback = std::bind(&Cell::onClicked, this, std::placeholders::_1);
    m_view->addEventListener(TouchEvent::CLICK, callback);
}

spActor Cell::getView() const
{
    return m_view;
}

void Cell::setOnClickedCallback(const onClickedCallback &callback)
{
    m_onClickedCallback = callback;
}

void Cell::highlight()
{
    spTweenQueue tweenQueue = new TweenQueue();

    tweenQueue->add(Sprite::TweenColor(Color::Red), 200, 1);
    tweenQueue->add(Sprite::TweenColor(computeColor()), 200, 1);

    m_view->addTween(tweenQueue);
}

void Cell::setSelected(bool isSelected)
{
    m_view->setColor(isSelected ? Color::LightBlue : computeColor());
}

void Cell::onClicked(Event *)
{
    m_onClickedCallback(m_boardCoordinate);
}

void Cell::initView(const Point &position, int cellSize)
{
    spColorRectSprite rectangle = new ColorRectSprite();
    rectangle->setSize(cellSize, cellSize);
    rectangle->setPosition(position);
    rectangle->setColor(computeColor());

    m_view = rectangle;
}

Color Cell::computeColor()
{
    return (size_t(m_boardCoordinate.x) + size_t(m_boardCoordinate.y)) % 2 ? Color::White : Color::Black;
}
