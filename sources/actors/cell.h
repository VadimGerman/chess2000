#pragma once

#include <functional>

#include "oxygine-framework.h"

DECLARE_SMART(Cell, spCell)

using onClickedCallback = std::function<void(const oxygine::Point &)>;

class Cell : public oxygine::Object
{
public:
    explicit Cell(const oxygine::Point &boardCoordinate, int cellSize);

    oxygine::spActor getView() const;
    void setOnClickedCallback(const onClickedCallback &callback);

    void highlight();
    void setSelected(bool isSelected);

private:
    void onClicked(oxygine::Event *event);
    void initView(const oxygine::Point &position, int cellSize);

    oxygine::Color computeColor();

private:
    onClickedCallback m_onClickedCallback;

    oxygine::Point m_boardCoordinate;

    oxygine::spColorRectSprite m_view;
};
