
#include "controllers/maincontroller.h"

#include "figure.h"

using namespace oxygine;

Figure::Figure(oxygine::Resources *resources,
               const oxygine::Point &boardCoordinate,
               const FigureInfo &info,
               int cellSize)
    : m_resources(resources),
      m_boardCoordinate(boardCoordinate),
      m_info(info),
      m_cellSize(cellSize)
{
    initView(computePosition(m_boardCoordinate));

    auto callback = std::bind(&Figure::onClicked, this, std::placeholders::_1);
    m_view->addEventListener(TouchEvent::CLICK, callback);
}

spActor Figure::getView() const
{
    return m_view;
}

Point Figure::getBoardCoordinate() const
{
    return m_boardCoordinate;
}

void Figure::setOnClickedCallback(const onClickedCallback &callback)
{
    m_onClickedCallback = callback;
}

void Figure::moveFigure(const oxygine::Point &boardCoordinate)
{
    m_boardCoordinate = boardCoordinate;
    m_view->addTween(Sprite::TweenPosition(computePosition(m_boardCoordinate)), 600, 1);
}

void Figure::killFigure()
{
    spTweenQueue tweenQueue = new TweenQueue();
    tweenQueue->setDelay(0);
    tweenQueue->add(Sprite::TweenAlpha(0), 400, 1);

    m_view->addTween(tweenQueue);

    tweenQueue->detachWhenDone();
}

void Figure::onClicked(Event *event)
{
    m_onClickedCallback(m_boardCoordinate);
}

void Figure::initView(const Point &positio)
{
    spSprite sprite = new Sprite();
    int column = int(m_info.type);
    int row = m_info.color == FigureInfo::White ? 1 : 0;
    sprite->setAnimFrame(m_resources->getResAnim("figures"), column, row);
    sprite->setPosition(positio);
    sprite->setSize(m_cellSize, m_cellSize);

    m_view = sprite;
}

Point Figure::computePosition(const oxygine::Point &point)
{
    int offsetX = point.x * 2;
    int offsetY = point.y * 2;
    int posX = point.x * m_cellSize + offsetX;
    int posY = point.y * m_cellSize + offsetY;

    return Point(posX, posY);
}
