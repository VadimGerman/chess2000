#pragma once

#include <functional>

#include "oxygine-framework.h"

#include "structs/figureinfo.h"

DECLARE_SMART(Figure, spFigure)

using onClickedCallback = std::function<void(const oxygine::Point &)>;

class Figure : public oxygine::Object
{
public:
    Figure(oxygine::Resources *resources, const oxygine::Point &boardCoordinate, const FigureInfo &info, int cellSize);

    oxygine::spActor getView() const;
    oxygine::Point getBoardCoordinate() const;
    void setOnClickedCallback(const onClickedCallback &callback);

    void moveFigure(const oxygine::Point &boardCoordinate);
    void killFigure();

private:
    void onClicked(oxygine::Event *event);
    void initView(const oxygine::Point &positio);

    oxygine::Point computePosition(const oxygine::Point &point);

private:
    oxygine::Resources *m_resources;
    oxygine::Point m_boardCoordinate;
    FigureInfo m_info;
    int m_cellSize;

    onClickedCallback m_onClickedCallback;
    oxygine::spSprite m_view;
};
