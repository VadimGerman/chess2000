
#include <functional>

#include "mainview.h"

using namespace oxygine;

MainView::MainView(Resources *resources, MainController *controller)
    : m_resources(resources),
      m_controller(controller)
{
    initBoard();
}

void MainView::onCellClicked(const oxygine::Point &newPosition)
{
    logs::messageln("clicked: %d, %d", newPosition.x, newPosition.y);

    MainController::ActionInfo actionInfo = m_controller->tapOnCell(newPosition);

    if (actionInfo.action == MainController::NoWay)
    {
        logs::messageln("NoWay");

        spCell cell = m_board.at(newPosition.x * MainModel::CellCount + newPosition.y);
        cell->highlight();
    }
    else if (actionInfo.action == MainController::Move || actionInfo.action == MainController::Kill)
    {
        if (actionInfo.action == MainController::Kill)
        {
            logs::messageln("Kill");

            auto iter = std::find_if(m_figures.begin(), m_figures.end(),
                                     [newPosition](spFigure &figure) {
                return figure->getBoardCoordinate() == newPosition;
            });

            if (iter == m_figures.end())
                return;

            (*iter)->killFigure();
            m_figures.erase(iter);
        }

        logs::messageln("Move");

        auto iter = std::find_if(m_figures.begin(), m_figures.end(),
                                 [actionInfo](spFigure &figure) {
            return figure->getBoardCoordinate() == actionInfo.selectedFigureBoardCoordinate;
        });

        if (iter == m_figures.end())
            return;

        (*iter)->moveFigure(newPosition);
    }
    else if (actionInfo.action == MainController::Select)
    {
        logs::messageln("Select");

        spCell cell = m_board.at(newPosition.x * MainModel::CellCount + newPosition.y);
        cell->setSelected(true);
        return;
    }
    else if (actionInfo.action == MainController::Unselect)
    {
        logs::messageln("Unselect");
    }
    else if (actionInfo.action == MainController::DoNothing)
    {
        logs::messageln("DoNothing");
        return;
    }

    spCell cell = m_board.at(actionInfo.selectedFigureBoardCoordinate.x * MainModel::CellCount
                             + actionInfo.selectedFigureBoardCoordinate.y);
    cell->setSelected(false);
}

void MainView::initBoard()
{
    auto addFigure = [this](const Point &point, const FigureInfo &info)
    {
        spFigure figure= new Figure(m_resources, point, info, CellSize);

        auto figureCallback = std::bind(&MainView::onCellClicked, this, std::placeholders::_1);
        figure->setOnClickedCallback(figureCallback);

        addChild(figure->getView());
        m_figures.push_back(figure);
    };

    auto callback = std::bind(&MainView::onCellClicked, this, std::placeholders::_1);

    // Init cells.
    for (int i = 0; i < MainModel::CellCount; ++i)
    {
        for (int j = 0; j < MainModel::CellCount; ++j)
        {
            spCell cell = new Cell(Point(i, j), CellSize);
            cell->setOnClickedCallback(callback);

            addChild(cell->getView());
            m_board[i * MainModel::CellCount + j] = cell;
        }
    }

    // Init figures.
    for (int i = 0; i < MainModel::CellCount; ++i)
    {
        for (int j = 0; j < MainModel::CellCount; ++j)
        {
            CellInfo info = m_controller->cellInfo(i, j);
            if (info.figure.type == FigureInfo::NONE)
                continue;

            addFigure(Point(i, j), info.figure);
        }
    }
}
