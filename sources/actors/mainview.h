#pragma once

#include <array>
#include "oxygine-framework.h"

#include "cell.h"
#include "figure.h"
#include "controllers/maincontroller.h"

class MainView : public oxygine::Actor
{
public:
    static constexpr int CellSize = 100;

public:
    MainView(oxygine::Resources *resources, MainController *controller);

private:
    void onCellClicked(const oxygine::Point &position);

    void initBoard();

private:
    oxygine::Resources *m_resources;
    MainController *m_controller;

    std::array<spCell, MainModel::CellCount * MainModel::CellCount> m_board;

    std::list<spFigure> m_figures;
};
