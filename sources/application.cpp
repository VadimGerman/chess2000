
#include "oxygine-framework.h"
#include <functional>
#include <array>

#include "application.h"

using namespace oxygine;

Application::Application()
    : m_model(),
      m_controller(&m_model)
{
    m_gameResources.loadXML("data/res.xml");

    m_board = new MainView(&m_gameResources, &m_controller);

    getStage()->addChild(m_board);
}

void Application::destroy()
{
    m_gameResources.free();
}
