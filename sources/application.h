#pragma once

#include "oxygine-framework.h"

#include "actors/mainview.h"
#include "controllers/maincontroller.h"
#include "models/mainmodel.h"

class Application
{
public:
    Application();

    void destroy();

private:
    MainModel m_model;
    MainController m_controller;
    oxygine::Resources m_gameResources;

    oxygine::intrusive_ptr<MainView> m_board;
};
