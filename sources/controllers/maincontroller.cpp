#include "maincontroller.h"

using namespace oxygine;

namespace {
const Point NotSelected = Point(-1, -1);
}

MainController::MainController(MainModel *model)
    : m_model(model),
      m_selectedFigureBoardCoordinate(NotSelected),
      m_playerTurnColor(FigureInfo::Color::White)
{
}

CellInfo MainController::cellInfo(int boardX, int boardY) const
{
    return m_model->cellInfo(boardX, boardY);
}

MainController::ActionInfo MainController::tapOnCell(const oxygine::Point &point)
{
    // Unselect.
    CellInfo tappedCellInfo = cellInfo(point.x, point.y);
    if (Point(tappedCellInfo.boardX, tappedCellInfo.boardY) == m_selectedFigureBoardCoordinate)
    {
        ActionInfo result(Action::Unselect, m_selectedFigureBoardCoordinate);
        m_selectedFigureBoardCoordinate = NotSelected;
        return result;
    }

    // Select/DoNothing/NoWay.
    if (m_selectedFigureBoardCoordinate == NotSelected)
    {
        if (tappedCellInfo.figure.type != FigureInfo::NONE)
        {
            if (m_playerTurnColor != tappedCellInfo.figure.color)
                return ActionInfo(Action::NoWay, point);

            m_selectedFigureBoardCoordinate = point;
            return ActionInfo(Action::Select, m_selectedFigureBoardCoordinate);
        }

        return ActionInfo(Action::DoNothing, m_selectedFigureBoardCoordinate);
    }

    // Move/Kill/NoWay.
    CellInfo selectedCellInfo = cellInfo(m_selectedFigureBoardCoordinate.x, m_selectedFigureBoardCoordinate.y);

    ActionInfo result(Action::NoWay, m_selectedFigureBoardCoordinate);

    switch (selectedCellInfo.figure.type)
    {
    case FigureInfo::FigureType::Pawn:
        result.action = computePawnWay(selectedCellInfo, tappedCellInfo);
        break;

    case FigureInfo::FigureType::Rook:
        result.action = computeRookWay(selectedCellInfo, tappedCellInfo);
        break;

    case FigureInfo::FigureType::Knight:
        result.action = computeKnightWay(selectedCellInfo, tappedCellInfo);
        break;

    case FigureInfo::FigureType::Bishop:
        result.action = computeBishopWay(selectedCellInfo, tappedCellInfo);
        break;

    case FigureInfo::FigureType::Queen:
        result.action = computeQueenWay(selectedCellInfo, tappedCellInfo);
        break;

    case FigureInfo::FigureType::King:
        result.action = computeKingWay(selectedCellInfo, tappedCellInfo);
        break;

    default:
        break;
    }

    if (result.action == Action::Kill && selectedCellInfo.figure.color == tappedCellInfo.figure.color)
        result.action = Action::NoWay;

    if (result.action == Action::NoWay)
        return result;
    else if (result.action == Action::Move || result.action == Action::Kill)
        m_model->moveFigure(m_selectedFigureBoardCoordinate.x, m_selectedFigureBoardCoordinate.y, point.x, point.y);

    m_selectedFigureBoardCoordinate = NotSelected;
    m_playerTurnColor = m_playerTurnColor == FigureInfo::Color::White ? FigureInfo::Color::Black
                                                                      : FigureInfo::Color::White;
    return result;
}

MainController::Action MainController::computeKingWay(CellInfo &from, CellInfo &to)
{
    int xDiff = std::max(from.boardX, to.boardX) - std::min(from.boardX, to.boardX);
    int yDiff = std::max(from.boardY, to.boardY) - std::min(from.boardY, to.boardY);

    if (xDiff <= 1 && yDiff <= 1)
    {
        if (to.figure.type == FigureInfo::FigureType::NONE)
            return Action::Move;

        return Action::Kill;
    }

    return Action::NoWay;
}

MainController::Action MainController::computeQueenWay(CellInfo &from, CellInfo &to)
{
    int xDiff = std::max(from.boardX, to.boardX) - std::min(from.boardX, to.boardX);
    int yDiff = std::max(from.boardY, to.boardY) - std::min(from.boardY, to.boardY);

    bool isHorizontalOrVerticalMove = (xDiff == 0 && yDiff > 0) || (yDiff == 0 && xDiff > 0);
    bool isDiagonalMove = xDiff == yDiff && xDiff > 0;
    if (isHorizontalOrVerticalMove || isDiagonalMove)
    {
        if (to.figure.type == FigureInfo::FigureType::NONE)
            return Action::Move;

        return Action::Kill;
    }

    return Action::NoWay;
}

MainController::Action MainController::computeBishopWay(CellInfo &from, CellInfo &to)
{
    int xDiff = std::max(from.boardX, to.boardX) - std::min(from.boardX, to.boardX);
    int yDiff = std::max(from.boardY, to.boardY) - std::min(from.boardY, to.boardY);

    bool isDiagonalMove = xDiff == yDiff && xDiff > 0;
    if (isDiagonalMove)
    {
        if (to.figure.type == FigureInfo::FigureType::NONE)
            return Action::Move;

        return Action::Kill;
    }

    return Action::NoWay;
}

MainController::Action MainController::computeRookWay(CellInfo &from, CellInfo &to)
{
    int xDiff = std::max(from.boardX, to.boardX) - std::min(from.boardX, to.boardX);
    int yDiff = std::max(from.boardY, to.boardY) - std::min(from.boardY, to.boardY);

    bool isHorizontalOrVerticalMove = (xDiff == 0 && yDiff > 0) || (yDiff == 0 && xDiff > 0);
    if (isHorizontalOrVerticalMove)
    {
        if (to.figure.type == FigureInfo::FigureType::NONE)
            return Action::Move;

        return Action::Kill;
    }

    return Action::NoWay;
}

MainController::Action MainController::computeKnightWay(CellInfo &from, CellInfo &to)
{
    int xDiff = std::max(from.boardX, to.boardX) - std::min(from.boardX, to.boardX);
    int yDiff = std::max(from.boardY, to.boardY) - std::min(from.boardY, to.boardY);

    if ((xDiff == 2 && yDiff == 1) || (yDiff == 2 && xDiff == 1))
    {
        if (to.figure.type == FigureInfo::FigureType::NONE)
            return Action::Move;

        return Action::Kill;
    }

    return Action::NoWay;
}

MainController::Action MainController::computePawnWay(CellInfo &from, CellInfo &to)
{
    int blackPawnInitRow = 1;
    int whitePawnInitRow = MainModel::CellCount - 2;
    bool isWhitePawn = from.figure.color == FigureInfo::Color::White;
    bool isOnInitPosition = isWhitePawn ? from.boardY == whitePawnInitRow
                                        : from.boardY == blackPawnInitRow;

    int xDiff = isWhitePawn ? from.boardY - to.boardY : to.boardY - from.boardY;
    if (xDiff < 1)
        return Action::NoWay;

    bool isXValidForKill = xDiff == 1;
    bool isXValidForMove = (xDiff < 3 && isOnInitPosition) || (xDiff < 2 && !isOnInitPosition);

    int yDiff = std::max(from.boardX, to.boardX) - std::min(from.boardX, to.boardX);
    bool isYValidForKill = yDiff == 1;
    bool isYValidForMove = yDiff == 0;
    if (isXValidForKill && isYValidForKill && to.figure.type != FigureInfo::FigureType::NONE)
        return Action::Kill;

    if (isYValidForMove && isXValidForMove && to.figure.type == FigureInfo::FigureType::NONE)
        return Action::Move;

    return Action::NoWay;
}

MainController::ActionInfo::ActionInfo(Action action,
                                       const Point &selectedFigureBoardCoordinate)
    : action(action),
      selectedFigureBoardCoordinate(selectedFigureBoardCoordinate)
{
}
