#pragma once

#include <array>

#include "oxygine/math/Vector2.h"

#include "structs/cellinfo.h"
#include "structs/figureinfo.h"
#include "models/mainmodel.h"

class MainController
{
public:
    enum Action
    {
        Select,
        Unselect,
        Move,
        Kill,
        DoNothing,
        NoWay,
    };

    struct ActionInfo
    {
        explicit ActionInfo(Action action,
                            const oxygine::Point &selectedFigureBoardCoordinate);

        Action action;
        oxygine::Point selectedFigureBoardCoordinate;
    };

public:
    MainController(MainModel *model);

    CellInfo cellInfo(int boardX, int boardY) const;

    ActionInfo tapOnCell(const oxygine::Point &point);

private:
    Action computeKingWay(CellInfo &from, CellInfo &to);
    Action computeQueenWay(CellInfo &from, CellInfo &to);
    Action computeBishopWay(CellInfo &from, CellInfo &to);
    Action computeRookWay(CellInfo &from, CellInfo &to);
    Action computeKnightWay(CellInfo &from, CellInfo &to);
    Action computePawnWay(CellInfo &from, CellInfo &to);

private:
    MainModel *m_model;

    oxygine::Point m_selectedFigureBoardCoordinate;

    FigureInfo::Color m_playerTurnColor;
};
