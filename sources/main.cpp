
#include "ox/oxygine.hpp"
#include "ox/Stage.hpp"
#include "ox/DebugActor.hpp"

#include "application.h"

using namespace oxygine;

const int WindowWidth = 860;
const int WindowHeight = 860;

bool mainloop(Application &app)
{
    bool done = core::update();

    getStage()->update();

    if (core::beginRendering())
    {
        Color clearColor(32, 32, 32, 255);
        Rect viewport(Point(0, 0), core::getDisplaySize());

        // Render all actors inside the stage. Actor::render will also be called for all its children
        getStage()->render(clearColor, viewport);

        core::swapDisplayBuffers();
    }

    return done;
}

void run()
{
    ObjectBase::__startTracingLeaks();

    core::init_desc desc;
    desc.title = "Chess2000";

    desc.w = WindowWidth;
    desc.h = WindowHeight;

    core::init(&desc);

    Stage::instance = new Stage();
    Point size = core::getDisplaySize();
    getStage()->setSize(size);

    Application app;

    bool isDone = false;
    do
    {
        isDone = mainloop(app);
    } while(!isDone);

    ObjectBase::dumpCreatedObjects();
    app.destroy();
    core::release();
    ObjectBase::dumpCreatedObjects();

    ObjectBase::__stopTracingLeaks();
}

#ifdef OXYGINE_SDL

#include "SDL_main.h"
#include "SDL.h"

extern "C"
{
    int main(int argc, char *argv[])
    {
        run();

        return 0;
    }
}
#endif
