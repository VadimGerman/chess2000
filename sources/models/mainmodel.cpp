#include "mainmodel.h"

MainModel::MainModel()
{
    initBoard();
    placeClassicBoardFigures();
}

CellInfo MainModel::cellInfo(int boardX, int boardY) const
{
    return m_board.at(boardX * CellCount + boardY);
}

void MainModel::moveFigure(int fromBoardX, int fromBoardY, int toBoardX, int toBoardY)
{
    CellInfo &from = m_board.at(fromBoardX * CellCount + fromBoardY);
    CellInfo &to = m_board.at(toBoardX * CellCount + toBoardY);

    to.figure = from.figure;
    from.figure = FigureInfo();
}

void MainModel::initBoard()
{
    for (int i = 0; i < CellCount; ++i)
    {
        for (int j = 0; j < CellCount; ++j)
        {
            CellInfo info(i, j, FigureInfo());
            m_board[i * CellCount + j] = info;
        }
    }
}

void MainModel::placeClassicBoardFigures()
{
    int lastRow = CellCount - 1;

    for (int i = 0; i < CellCount; ++i)
    {
        m_board[CellCount * i + 1].figure = FigureInfo(FigureInfo::Black, FigureInfo::Pawn);
        m_board[CellCount * i + lastRow - 1].figure = FigureInfo(FigureInfo::White, FigureInfo::Pawn);
    }

    m_board[CellCount * 0].figure = FigureInfo(FigureInfo::Black, FigureInfo::Rook);
    m_board[CellCount * 1].figure = FigureInfo(FigureInfo::Black, FigureInfo::Knight);
    m_board[CellCount * 2].figure = FigureInfo(FigureInfo::Black, FigureInfo::Bishop);
    m_board[CellCount * 3].figure = FigureInfo(FigureInfo::Black, FigureInfo::Queen);
    m_board[CellCount * 4].figure = FigureInfo(FigureInfo::Black, FigureInfo::King);
    m_board[CellCount * 5].figure = FigureInfo(FigureInfo::Black, FigureInfo::Bishop);
    m_board[CellCount * 6].figure = FigureInfo(FigureInfo::Black, FigureInfo::Knight);
    m_board[CellCount * 7].figure = FigureInfo(FigureInfo::Black, FigureInfo::Rook);

    m_board[CellCount * 0 + lastRow].figure = FigureInfo(FigureInfo::White, FigureInfo::Rook);
    m_board[CellCount * 1 + lastRow].figure = FigureInfo(FigureInfo::White, FigureInfo::Knight);
    m_board[CellCount * 2 + lastRow].figure = FigureInfo(FigureInfo::White, FigureInfo::Bishop);
    m_board[CellCount * 3 + lastRow].figure = FigureInfo(FigureInfo::White, FigureInfo::Queen);
    m_board[CellCount * 4 + lastRow].figure = FigureInfo(FigureInfo::White, FigureInfo::King);
    m_board[CellCount * 5 + lastRow].figure = FigureInfo(FigureInfo::White, FigureInfo::Bishop);
    m_board[CellCount * 6 + lastRow].figure = FigureInfo(FigureInfo::White, FigureInfo::Knight);
    m_board[CellCount * 7 + lastRow].figure = FigureInfo(FigureInfo::White, FigureInfo::Rook);
}
