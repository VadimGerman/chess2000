#pragma once

#include <array>

#include "structs/cellinfo.h"

class MainModel
{
public:
    static constexpr int CellCount = 8;

public:
    explicit MainModel();

    CellInfo cellInfo(int boardX, int boardY) const;
    void moveFigure(int fromBoardX, int fromBoardY, int toBoardX, int toBoardY);

private:
    void initBoard();
    void placeClassicBoardFigures();

private:
    std::array<CellInfo, CellCount * CellCount> m_board;
};
