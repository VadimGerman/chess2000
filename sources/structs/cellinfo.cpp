#include "cellinfo.h"

CellInfo::CellInfo(int boardX,
                   int boardY,
                   const FigureInfo &figure)
    : boardX(boardX),
      boardY(boardY),
      figure(figure)
{
}
