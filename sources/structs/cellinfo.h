#pragma once

#include "figureinfo.h"

struct CellInfo
{
    explicit CellInfo() = default;
    explicit CellInfo(int boardX, int boardY, const FigureInfo &figure);

    int boardX;
    int boardY;
    FigureInfo figure;
};
