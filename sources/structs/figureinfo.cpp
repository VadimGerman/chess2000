#include "figureinfo.h"

FigureInfo::FigureInfo()
    : type(NONE)
{
}

FigureInfo::FigureInfo(Color color, FigureType type)
    : color(color),
      type(type)
{
}
