#pragma once

struct FigureInfo
{
    enum FigureType {
        NONE = -1,

        King = 0,
        Queen = 1,
        Rook = 2,
        Knight = 3,
        Bishop = 4,
        Pawn = 5,
    };

    enum Color {
        White = 0,
        Black,
    };

    explicit FigureInfo();
    explicit FigureInfo(Color color, FigureType type);

    Color color;
    FigureType type;
};
